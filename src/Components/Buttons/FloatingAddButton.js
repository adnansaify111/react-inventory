import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';

export const FloatingAddButton = style => {
  return (
    <Fab
      color="primary"
      aria-label="Add"
      style={style}
      onClick={() => {
        this.props.history.push(`home/add-sale`);
      }}
    >
      <AddIcon />
    </Fab>
  );
};
