import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import { firebaseApp } from '../../Database/firebaseApp';
// import { AddNewProduct } from "../../Store/Actions/MiddleWare";
import { addProduct } from '../../Store/Actions/ProductActions';

import TextField from 'material-ui/TextField';
import { withStyles } from '@material-ui/core';
import AppBar from 'material-ui/AppBar';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { styles } from '../AdminDashboard/styles';

class AddProduct extends Component {
  constructor() {
    super();
    this.state = {
      productName: '',
      description: '',
      company: '',
      price: null,
      qty: 0,
    };
    this.submit = this.submit.bind(this);
    this.inputHandler = this.inputHandler.bind(this);
  }
  inputHandler(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }
  submit(e) {
    e.preventDefault();
    let multipath = {};
    let productDetails = {
      productName: this.state.productName,
      description: this.state.description,
      quantity: this.state.company,
      price: this.state.price,
    };

    let stock = {
      productName: this.state.productName,
      qty: this.state.qty,
    };
    {
      this.props.AddProductRequest(productDetails);
    }
    console.log('productDetails', productDetails);
  }

  render() {
    return (
      <div>
        <center>
          <h1>Add New Product</h1>
          <form onSubmit={this.submit}>
            <TextField hintText="Product Name" name="productName" value={this.state.productName} floatingLabelText="Product Name" onChange={this.inputHandler} />
            <br />
            <br />
            <TextField type="text" hintText="description" name="description" value={this.state.description} floatingLabelText="description" onChange={this.inputHandler} />
            <br />
            <br />
            <TextField type="text" hintText="company" name="company" value={this.state.company} floatingLabelText="company" onChange={this.inputHandler} />
            <br />
            <br />
            <TextField type="text" hintText="price" name="price" value={this.state.price} floatingLabelText="price" onChange={this.inputHandler} />
            <br />
            <br />
            <RaisedButton type="submit" label="Add Product" primary={true} secondary={false} classes={styles.addButton} /> <br />
            <br />
          </form>
        </center>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log('###state', state);
  return {
    productReducer: state.productReducer,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    AddProductRequest: data => {
      dispatch(addProduct(data));
    },
  };
};

export default withStyles(styles, { withTheme: true })(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(AddProduct),
);
