import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DatePicker from 'material-ui/DatePicker';
import Divider from '@material-ui/core/Divider';

import { AddNewSale, ViewProducts, ViewStores } from '../../Store/Actions/MiddleWare';
import { withStyles, ListItem, List, ListItemText, ListItemIcon } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    width: 500,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 200,
  },
});
const handleChange = () => {
  console.log('test');
};
class ProductsBilling extends Component {
  constructor(props) {
    super();
    this.state = {};
  }

  render() {
    const { classes, billedProducts } = this.props;
    const { ser_term } = this.state;

    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={12} lg={12}>
            <h1>Billing</h1>
            <Divider />
          </Grid>

          <Grid item xs={12} lg={12}>
            <div className={classes.paper}>
              <ListItem
                button
                type="submit"
                key="overview"
                text-align="center"
                value="overview"
                onClick={() => this.handleChange}
              >
                <ListItemIcon>
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M5.88 4.12L13.76 12l-7.88 7.88L8 22l10-10L8 2z" />
                    <path fill="none" d="M0 0h24v24H0z" />
                  </svg>
                </ListItemIcon>
                <ListItemText primary="Overview" />
              </ListItem>
            </div>
            <Divider />
          </Grid>
          <Grid item xs={12} lg={6}>
            <div className={classes.paper}>
              <Paper className={classes.paper}>test</Paper>
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log('state', state);
  return {
    allProducts: state.productReducer.products,
    allStores: state.storeReducer.stores,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addSaleRequest: data => {
      dispatch(AddNewSale(data));
    },
    ...bindActionCreators({ ViewProducts, ViewStores }, dispatch),
  };
};

export default withStyles(styles, { withTheme: true })(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ProductsBilling),
);
