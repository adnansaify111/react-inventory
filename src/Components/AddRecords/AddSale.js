import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DatePicker from 'material-ui/DatePicker';
import Divider from '@material-ui/core/Divider';
import { AddNewSale, ViewProducts, ViewStores } from '../../Store/Actions/MiddleWare';
import { withStyles } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import ProductsBilling from './ProductsBillling';
import { FilterdProductsList } from './FilterdProductsList';

const style = {
  padding: '10px',
  textAlign: 'center',
};

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    width: 500,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 200,
  },
});
class AddSaleOrder extends Component {
  constructor(props) {
    super();
    this.state = {
      productId: '',
      productName: '',
      description: '',
      qty: 0,
      unitPrice: 0,
      storeName: '',
      storeId: '',
      saleDate: '',
      ser_term: '',
      filteredProducts: [],
      billedProducts: [],
    };

    props.ViewProducts();
    props.ViewStores();
    this.submit = this.submit.bind(this);
    this.inputHandler = this.inputHandler.bind(this);
  }
  inputHandler(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  submit(e) {
    e.preventDefault();
    let productDetails = {
      productName: '',
      storeName: '',
      productId: this.state.productId,
      storeId: this.state.storeId,
      description: this.state.description,
      qty: this.state.qty,
      unitPrice: this.state.unitPrice,
      saleDate: this.state.saleDate,
    };
    console.log(productDetails);
    console.log(this.props.history);
    this.props.addSaleRequest(productDetails);
  }

  handleDateChange = (event, date) => {
    this.setState({
      saleDate: date,
    });
  };

  handleOnSelect = productId => {
    const id = productId;
    const selectedProducts = this.state.filteredProducts.filter(products => {
      return products === id;
    });
    const { buildProducts } = this.state;
    if (buildProducts) {
      buildProducts.push(selectedProducts);
      this.setState({ buildProducts });
    }
  };
  handleChange = evt => {
    const ser_term = evt.target.value;
    const query_term = new RegExp(ser_term, 'i');
    this.setState({ ser_term });
    const filteredProducts = this.props.allStores.filter(item => {
      return query_term.test(item.storeName);
    });
    console.log('###filtered products', filteredProducts);
    this.setState({ filteredProducts });

    console.log(evt);
  };

  render() {
    const { classes } = this.props;
    const { ser_term, filteredProducts, billedProducts } = this.state;
    console.log('filteredProducts', filteredProducts);
    const filteredProductsList = filteredProducts.map(item => {
      return <FilterdProductsList products={item} handleOnSelect={this.handleOnSelect} />;
    });
    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <Grid item xs={12} lg={12}>
            <h1>Sell Prodcuts</h1>
          </Grid>
          <Grid item xs={12} lg={6}>
            <div className={classes.paper}>
              <form className={classes.container} noValidate autoComplete="off" onSubmit={this.submit}>
                <TextField
                  id="outlined-name"
                  label="Search for products"
                  className={classes.textField}
                  value={ser_term}
                  onChange={evt => this.handleChange(evt)}
                  margin="normal"
                  variant="outlined"
                />
              </form>
              <div>{filteredProductsList}</div>
            </div>
          </Grid>
          <Grid item xs={12} lg={6}>
            <div className={classes.paper}>
              <Paper className={classes.paper}>
                <ProductsBilling billedProducts={billedProducts} />
              </Paper>
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log('state', state);
  return {
    allProducts: state.productReducer.products,
    allStores: state.storeReducer.stores,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addSaleRequest: data => {
      dispatch(AddNewSale(data));
    },
    ...bindActionCreators({ ViewProducts, ViewStores }, dispatch),
  };
};

export default withStyles(styles, { withTheme: true })(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(AddSaleOrder),
);
