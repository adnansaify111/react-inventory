import { createStyles } from '@material-ui/core';

const drawerWidth = 178;
export const styles = theme =>
  createStyles({
    root: {
      display: 'flex',
      backgroundColor: '#eff4f4',
    },
    drawer: {
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    appBar: {
      marginLeft: drawerWidth,
      backgroundColor: '#3a4a55',
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
      },
    },
    menuButton: {
      marginRight: 20,
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },

    onOpenmenuButton: {
      display: 'none',
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      padding: '0 8px',
      ...theme.mixins.toolbar,
      justifyContent: 'center',
      width: '100%',
    },
    drawerPaper: {
      width: drawerWidth,
      backgroundColor: '#fff',
      '&::-webkit-scrollbar': {
        backgroundColor: '#F4F5F7',
        width: '4px',
      },
      '&::-webkit-scrollbar-thumb': {
        backgroundColor: '#fffff',
        width: '6px',
      },
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing.unit * 3,
    },
    oncontentopen: {
      //marginLeft: `${drawerWidth}px`,
      [theme.breakpoints.up('lg')]: {
        marginLeft: `${drawerWidth}px`,
        width: '86%',
      },
    },

    nested: {
      paddingLeft: theme.spacing.unit * 0,
    },

    ListitemText: {
      paddingLeft: '20px !important',
    },

    ListItem: {
      '&:hover': {
        backgroundColor: '#ffff',
      },
    },
    homebutton: {
      textDecoration: 'none',
      cursor: 'pointer',
      color: 'white',
    },

    onopenhomebutton: {
      textDecoration: 'none',
      cursor: 'pointer',
      color: 'white',
      marginLeft: `${drawerWidth - 10}px`,
      marginRight: 0,
    },
    drawerHeader: {
      '&:hover': {
        backgroundColor: '#eff4f4',
      },
      display: 'flex',
      alignItems: 'center',
      padding: '0 8px',
      ...theme.mixins.toolbar,
      justifyContent: 'flex-end',
    },
    hide: {
      display: 'none',
    },
    active: {
      backgroundColor: '#eff4f4',
      '&:hover': {
        backgroundColor: '#eff4f4',
      },
    },

    subitemsactive: {
      backgroundColor: '#eff4f4',
      '&:hover': {
        backgroundColor: '#eff4f4',
      },
    },

    addButton: {
      backgroundColor: 'green',
    },
  });
