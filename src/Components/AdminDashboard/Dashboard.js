import React from 'react';

import classNames from 'classnames';
import {
  AppBar,
  Toolbar,
  Drawer,
  withStyles,
  IconButton,
  Hidden,
  ListItem,
  List,
  ListItemText,
  Collapse,
} from '@material-ui/core';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import items from './Menuitems.json';
import RaisedButton from 'material-ui/RaisedButton';

import { Link } from 'react-router-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { LogOut } from '../../Store/Actions/MiddleWare';
import { connect } from 'react-redux';

import AddProduct from '../AddRecords/AddProduct';
import ViewProducts from '../Reports/ViewProducts';
import AddPurchase from '../AddRecords/AddPurchase';
import AddSale from '../AddRecords/AddSale';
import AddStore from '../AddRecords/AddStore';
import ViewStores from '../Reports/ViewStores';
import ViewPurchases from '../Reports/ViewPurchases.js';
import ViewSales from '../Reports/ViewSales';
import Stock from '../Stock/Stock';
import { styles } from './styles';

class Dashboard extends React.Component {
  state = {
    mobileOpen: false,
    openItem: 'null',
    selectedPackage: 'null',
    toggledropdown: false,
    desktopopen: false,
  };
  handleclick(value) {
    console.log(value);
    if (this.state.openItem !== value) {
      this.setState({ openItem: `${value}`, toggledropdown: true });
    } else {
      this.setState({
        openItem: `${value}`,
        toggledropdown: !this.state.toggledropdown,
      });
    }
  }

  renderComponent(value, name) {
    console.log(value);
    this.props.history.push(`/home/${value}`);

    this.setState({
      selectedPackage: `${name}`,
      mobileOpen: false,
      desktopopen: false,
    });
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  logOut = () => {
    this.props.LogOutRequest().then(this.props.history.push('/login'));
  };

  render() {
    const { classes, theme } = this.props;
    const state = this.state;
    console.log('###history', this.props.history);

    const drawer = (
      <div>
        <div
          className={classNames(classes.toolbar, {
            [classes.drawerHeader]: state.mobileOpen === true,
          })}
        >
          <Typography variant="h6" color="inherit" noWrap={true} align="justify">
            Dashboard
          </Typography>
          <div>
            <IconButton
              className={classes.drawerHeader}
              onClick={() => this.setState({ mobileOpen: false, desktopopen: false })}
            >
              <ChevronLeftIcon />
            </IconButton>
          </div>
        </div>
        <Divider />
        <div>
          <ListItem
            button
            type="submit"
            key="overview"
            text-align="center"
            value="overview"
            onClick={() => this.handleclick('package1')}
          >
            <ListItemText primary="Overview" />
          </ListItem>
        </div>
        {items.map(list => (
          <div>
            <ListItem
              className={classNames(classes.ListItem, { [classes.active]: this.state.openItem === list.title })}
              button
              key={`${list.id}`}
              onClick={() => this.handleclick(`${list.title}`)}
            >
              <ListItemText primary={`${list.title}`} />
              {state.openItem === `${list.title}` && state.toggledropdown === true ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse
              key={list.subitems.name}
              in={state.openItem === `${list.title}` && state.toggledropdown === true ? true : false}
              timeout="auto"
              unmountOnExit
              className={classes.nested}
            >
              <List component="div" disablePadding>
                {list.subitems.map(subitems => (
                  <div>
                    <ListItem
                      className={classNames(classes.ListItem, {
                        [classes.subitemsactive]: state.selectedPackage === subitems.name,
                      })}
                      button
                      onClick={() => this.renderComponent(`${subitems.key}`, `${subitems.name}`)}
                    >
                      <ListItemText className={classes.ListitemText} primary={`${subitems.name}`} />
                    </ListItem>
                  </div>
                ))}
              </List>
            </Collapse>
          </div>
        ))}
        <Divider />
      </div>
    );

    return (
      <div className={classes.root}>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              onClick={() => this.setState({ mobileOpen: true, desktopopen: true })}
              className={classNames(classes.menuButton, {
                [classes.onOpenmenuButton]: this.state.mobileOpen === true || this.state.desktopopen === true,
              })}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap={true} style={{ width: '92%' }}>
              <Link
                className={classNames(classes.homebutton, {
                  [classes.onopenhomebutton]: this.state.mobileOpen === true || this.state.desktopopen === true,
                })}
                to="/home"
              >
                React Tutorial
              </Link>
            </Typography>
            <RaisedButton onClick={this.logOut} label="Logout" primary={false} />
          </Toolbar>
        </AppBar>

        <nav className={classes.drawer}>
          <Hidden smUp implementation="css">
            <Drawer
              container={this.props.container}
              variant="temporary"
              anchor={theme.direction === 'rtl' ? 'right' : 'left'}
              open={this.state.mobileOpen}
              onClose={this.handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper,
              }}
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden xsDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper,
              }}
              variant="permanent"
              open
            >
              {drawer}
            </Drawer>
          </Hidden>
        </nav>

        <main
          // className= {classes.oncontentopen}
          className={classNames(
            { [classes.content]: this.state.desktopopen === false },
            { [classes.oncontentopen]: this.state.desktopopen === true },
          )}
        >
          <div className={classes.toolbar} />
          <Switch>
            <Route exact path="/home/add-product" component={AddProduct}>
              {' '}
            </Route>
            <Route path="/home/add-purchase" component={AddPurchase}>
              {' '}
            </Route>
            <Route path="/home/add-sale" component={AddSale}>
              {' '}
            </Route>
            <Route path="/home/add-store" component={AddStore}>
              {' '}
            </Route>
            <Route exact path="/home/view-products" component={ViewProducts}>
              {' '}
            </Route>
            <Route path="/home/view-purchases" component={ViewPurchases}>
              {' '}
            </Route>
            <Route path="/home/view-sales" component={ViewSales}>
              {' '}
            </Route>
            <Route path="/home/view-stores" component={ViewStores}>
              {' '}
            </Route>
            <Route path="/home/view-stock" component={Stock}>
              {' '}
            </Route>
          </Switch>
        </main>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    //storeReducer: state.storeReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    LogOutRequest: () => {
      dispatch(LogOut());
    },
  };
};

export default withStyles(styles, { withTheme: true })(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Dashboard),
);
