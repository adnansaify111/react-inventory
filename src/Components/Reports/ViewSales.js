import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import { connect } from 'react-redux';
import Snackbar from 'material-ui/Snackbar';
import { ViewSales } from '../../Store/Actions/MiddleWare';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import { FloatingAddButton } from '../Buttons/FloatingAddButton';
import CustomToolbarSelect from './CustomToolbarSelect';

import MUIDataTable from 'mui-datatables';

const style = {
  margin: 20,
  display: 'block-inline',
  height: 'auto',
  width: 'auto',
  padding: 20,
  backgroundColor: '#9CCC65',
};

const AddStyle = {
  margin: 0,
  top: 'auto',
  right: 20,
  bottom: 20,
  left: 'auto',
  position: 'fixed',
};
class ViewProducts extends Component {
  constructor(props) {
    super();
    this.state = {
      open: false,
    };
    props.loadSales();
  }

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  componentWillReceiveProps() {
    this.setState({
      open: this.props.snackbarStatus,
    });
  }

  handleAdd = value => {
    console.log(value.rowData);
  };
  handleTest = () => {
    console.log('test');
  };
  render() {
    const options = {
      responsive: 'stacked',
      filterType: 'checkbox',
      onRowsDelete: rowsDeleted => {
        console.log(rowsDeleted);
      },
    };
    const columns = [
      'Product name',
      'Description',
      'Quantity',
      {
        name: 'New',
        options: {
          selectableRows: true,
          showResponsive: true,
          filter: true,
          customToolbarSelect: (selectedRows, displayData, setSelectedRows) => {
            return 'test';
          },
          customBodyRender: (value, tableMeta, updateValue) => {
            return (
              <Fab
                color="primary"
                aria-label="Add"
                onClick={event => {
                  this.handleAdd(tableMeta);
                  console.log('###', tableMeta);
                  // updateValue(event.target.value === "Yes" ? false : true);
                }}
              >
                <AddIcon />
              </Fab>
            );
          },
        },
      },
    ];
    const { allSales, history } = this.props;
    let tableData = allSales.map(item => {
      return [item.productName, item.description, item.qty, 'test'];
    });
    console.log(tableData);
    return (
      <div>
        <center>
          <h1>Sale Orders</h1>
          <br />
          <br />
        </center>
        <MUIDataTable title={'Sales'} data={tableData} columns={columns} options={options} />
        {/* <Fab
          color="primary"
          aria-label="Add"
          style={Addstyle}
          onClick={() => {this.props.history.push(`home/add-sale`)}}
        
        >
          <AddIcon />
        </Fab> */}
        <Fab
          color="primary"
          aria-label="Add"
          style={AddStyle}
          onClick={() => {
            this.props.history.push(`/add-sale`);
          }}
        >
          <AddIcon />
        </Fab>
        <Snackbar
          open={this.state.open}
          message="Stock Subtracted Successfully!"
          autoHideDuration={4000}
          onRequestClose={this.handleRequestClose}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(state.saleReducer.sales);
  return {
    allSales: state.saleReducer.sales,
    snackbarStatus: state.saleReducer.open,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadSales: () => {
      dispatch(ViewSales());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ViewProducts);
