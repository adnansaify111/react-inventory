import * as firebase from 'firebase';
import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import { connect } from 'react-redux';
// import { ViewProducts } from "../../Store/Actions/MiddleWare";
import { viewProduct } from '../../Store/Actions/ProductActions';

import MUIDataTable from 'mui-datatables';

const style = {
  margin: 20,
  display: 'block-inline',
  height: 'auto',
  width: 'auto',
  padding: 20,
  backgroundColor: '#E6EE9C',
};

class ViewProductsList extends Component {
  constructor() {
    super();
  }

  componentWillMount() {
    {
      this.props.searchProducts();
    }
  }

  render() {
    const { products } = this.props;
    console.log('###test', products);
    const columns = ['Product name', 'Description', 'Brand', 'Quantity'];
    let tableData = products.map(item => {
      return [item.productName, item.description, item.company, item.qty];
    });

    return (
      <div>
        <center>
          <h1>Products List</h1>
          <br />
          <br />
        </center>
        <MUIDataTable title={'Product List'} data={tableData} columns={columns} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log('state', state);
  return {
    products: state.productReducer.products,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    searchProducts: () => {
      dispatch(viewProduct());
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ViewProductsList);
