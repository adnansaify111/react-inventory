export function productReducer(
  state = {
    hasError: false,
    errorMessage: {},
    hasLoggedIn: false,
    hasRegistered: false,
    products: [],
  },
  action,
) {
  switch (action.type) {
    case 'ADD_PRODUCTS': {
      return {
        ...state,
        fetching: false,
        fetched: true,
        products: action.payload,
      };
    }
    case 'ADD_PRODUCTS_SUCCESS': {
      console.log('product added successfully');
      let newState = { ...state };
      newState.products = action.payload;
      return newState;
    }

    case 'ADD_PRODUCTS_FAILED': {
      console.log('product added failed');
      let newState = { ...state };
      newState.products = action.error;
      return newState;
    }

    case 'VIEW_PRODUCTS': {
      return {
        ...state,
        fetching: false,
        fetched: true,
        products: action.payload,
      };
    }
    case 'VIEW_PRODUCTS_SUCCESS': {
      console.log('product added successfully');
      return Object.assign({}, state, { products: action.payload });
    }

    case 'VIEW_PRODUCTS_FAILED': {
      console.log('product added failed');
      let newState = { ...state };
      newState.products = action.error;
      return newState;
    }
  }

  return state;
}
