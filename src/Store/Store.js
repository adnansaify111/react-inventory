import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { authReducer, storeReducer, purchaseReducer, saleReducer, stockReducer } from './Reducers/reducer';
import { productReducer } from './Reducers/ProductsReducer';

const rootReducer = combineReducers({
  authReducer,
  productReducer,
  storeReducer,
  purchaseReducer,
  saleReducer,
  stockReducer,
});
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const logger = createLogger();

let store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk, logger)));

export default store;
