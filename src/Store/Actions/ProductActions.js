import api from '../../utilities/api';

export function addProduct(data) {
  return function(dispatch) {
    dispatch({ type: 'ADD_PRODUCTS' });
    api
      .post('/products/create', data)
      .then(response => {
        dispatch({ type: 'ADD_PRODUCTS_SUCCESS' });
      })
      .catch(err => {
        dispatch({ type: 'ADD_PRODUCTS_FAILED', payload: err });
      });
  };
}

export function viewProduct() {
  console.log('###data in action view product');
  return function(dispatch) {
    dispatch({ type: 'VIEW_PRODUCTS' });
    api
      .get('/products/create')
      .then(response => {
        dispatch({ type: 'VIEW_PRODUCTS_SUCCESS', payload: response.data });
      })
      .then(console.log('###history', history))
      .catch(err => {
        dispatch({ type: 'VIEW_PRODUCTS_FAILED', payload: err });
      });
  };
}
